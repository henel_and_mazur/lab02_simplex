import logging
from saport.simplex.model import Model 

def create_model() -> Model:
    model = Model(__file__)

    #TODO:
    # fill missing test based on the example_01_solvable.py
    # to make the test a bit more interesting:
    # * make the model unbounded!
    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")
    x4 = model.create_variable("x4")

    model.add_constraint(5 * x1 - x2 - x4 == 0)
    model.add_constraint(x1 <= 250)
    model.add_constraint(x4 - x1 == 200)

    model.maximize(4 * x1 - 2 * x2 + 1 * x3 + 0 * x4)

    return model 

def run():
    model = create_model()
    #TODO:
    # add a test "assert something" based on the example_01_solvable.py
    #
    # TIP: you may use other solvers (e.g. https://online-optimizer.appspot.com)
    #      to find the correct solution
    logging.info("This test is empty but it shouldn't be, fix it!")
    raise AssertionError("Test is empty")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()
